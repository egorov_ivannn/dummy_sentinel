docker network create redis
sh run_redis.sh
sh run_sentinel.sh

docker build . -t worker:latest
docker run -it --net redis worker:latest