
from celery import Celery
from celery import current_task
from celery.signals import worker_process_init


REDIS_PASSWORD = 'a-very-complex-password-here'
REDIS_SENTINEL_PASSWORD = 'a-very-complex-password-here'


# redis_broker = f'sentinel://localhost:5000;sentinel://localhost:5001;sentinel://localhost:5002'
# redis_backend = f'sentinel://:{REDIS_PASSWORD}@localhost:5000;sentinel://:{REDIS_PASSWORD}@localhost:5001;sentinel://:{REDIS_PASSWORD}@localhost:5002'

redis_broker = f'sentinel://:{REDIS_PASSWORD}@sentinel-0:5000;sentinel://:{REDIS_PASSWORD}@sentinel-1:5001;sentinel://:{REDIS_PASSWORD}@sentinel-2:5002'
# redis_broker = f'sentinel://:{REDIS_PASSWORD}@sentinel-0:5000'
# redis_broker = f'sentinel://sentinel-0:5000;sentinel://sentinel-1:5001;sentinel://sentinel-2:5002'
# redis_backend = f'sentinel://:{REDIS_PASSWORD}@sentinel-0:5000;sentinel://:{REDIS_PASSWORD}@sentinel-1:5001;sentinel://:{REDIS_PASSWORD}@sentinel-2:5002'

app_celery = Celery('app_celery')
app_celery.conf.broker_url = redis_broker
app_celery.conf.broker_transport_options = {'master_name': 'mymaster', 'sentinel_kwargs': { 'password': REDIS_SENTINEL_PASSWORD }}