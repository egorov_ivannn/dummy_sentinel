cd clustering

docker run -d --rm --name redis-0 \
    --net redis \
    -v redis-0:/etc/redis/ \
    redis:6.0-alpine redis-server /etc/redis/redis.conf

#redis-1
docker run -d --rm --name redis-1 \
    --net redis \
    -v redis-1:/etc/redis/ \
    redis:6.0-alpine redis-server /etc/redis/redis.conf


#redis-2
docker run -d --rm --name redis-2 \
    --net redis \
    -v redis-2:/etc/redis/ \
    redis:6.0-alpine redis-server /etc/redis/redis.conf
