FROM python:latest

WORKDIR /src/

COPY requirements.txt /src/
COPY tasker.py /src/
RUN pip install --upgrade pip \
 && pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["celery", "-A", "tasker", "worker", "-c", "1", "--loglevel=ERROR" ]